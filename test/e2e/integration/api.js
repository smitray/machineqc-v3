/* eslint-disable */

describe('Testing machine api (GET): /machines', () => {
  let result;

  let options = {
    url: 'http://localhost:3000/api/machines',
    method: 'GET',
    qs: {
      pageNum: 1,
      pageSize: 5
    }
  };

  it('Validate the header', () => {
    options.qs.state = 'qc';
    result = cy.request(options);
    result.its('headers')
          .its('content-type')
          .should('include', 'application/json')
    
  });

  it('Validate the status', () => {
    options.qs.state = 'qc';
    result = cy.request(options);
    result.its('status')
          .should('equal', 200)
  });

  it('Validate data is not null', () => {
    options.qs.state = 'qc';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on page change', () => {
    options.qs.state = 'qc';
    options.qs.pageNum = 2;
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on page size change', () => {
    options.qs.state = 'qc';
    options.qs.pageSize = 10;
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: registered', () => {
    options.qs.state = 'registered';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: qc_ready', () => {
    options.qs.state = 'qc_ready';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: qc_started', () => {
    options.qs.state = 'qc_started';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: qc_restarted', () => {
    options.qs.state = 'qc_restarted';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: qc_stopped', () => {
    options.qs.state = 'qc_stopped';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: qc_passed', () => {
    options.qs.state = 'qc_passed';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: qc_failed', () => {
    options.qs.state = 'qc_failed';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response on status change: transferred', () => {
    options.qs.state = 'transferred';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response for a specific date', () => {
    options.qs.state = 'qc';
    options.qs.createdOn = '2017-07-20';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response for model number search', () => {
    options.qs.state = 'qc';
    options.qs.modelNo = 'svp';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response for vendor name search', () => {
    options.qs.state = 'qc';
    options.qs.vendorName = 'vpd';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

  it('Validate data response for company search and valid for only transferred machines', () => {
    options.qs.state = 'transferred';
    options.qs.company = 'nova';
    cy.request(options).then((resp) => {
      expect(resp.machines).to.not.be.null;
      expect(resp.total).to.not.be.null;
    });
  });

});
