[![Codeship Status for smitray/machineqc-v3](https://app.codeship.com/projects/b2a25880-90d2-0137-64fe-5a087f3115e8/status?branch=master)](https://app.codeship.com/projects/356273)

# MachineQC V3


### Libraries and versions

1. Nuxt.js (Vue based SSR) : 2.8.1
2. Express js: 4.17.x
3. Axios: 0.19.0
4. Vuetify: 2.x

### Build progress
In our first build we are delivering the first milestone which contains the 2 modules of the application.

1. QC Machines
2. Transferred machines

Both the modules contain few filter, which are:

1. Serial Number
2. Model Number
3. Vendor
4. Created on
5. Status
6. Company

It also contains server side pagination. Since we don't intend to make the DOM heavy, we're keeping most of the data in server and loading it as required.

### Folder structure

#### api
This folder contains all the server side coding and implementation of DAO

#### layout
Here you can find all the file that contains the layout structure

#### pages
Here you can find the individual pages

#### components
This folder contains all the common components used in this application

### Design pattern
We used Vuetify as requested thus Material design pattern has been followed

### DAO implementation


```javascript
const bytenode = require('bytenode');
const Dao = require('../dao/dao.jsc');


export const getDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.getGenericWOLoop(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const deleteDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.deletecall(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});
```

Config

```javascript

module.exports = {
  "getAllMachines": {
    "api": "http://52.224.181.203:5000/api/machineqc",
    "mode": "accesspath",
    "accessPath": "$[*]",
    "accessMap": {

    }
  },
  "getAllCompanies": {
    "api": "http://52.224.181.203:5000/api/company",
    "mode": "accesspath",
    "accessPath": "$[*]",
    "accessMap": {

    }
  },
  "deleteMachine": {
    "api": "http://52.224.181.203:5000/api/machineqc/#machineId",
    "apiVars": {
      "#machineId": "$.machineId"
    },
    "accessPath": "$[*]",
    "accessMap": {
      
    }
  },
  "getSingleMachine": {
    "api": "http://52.224.181.203:5000/api/machine/#machineId?&filter[include][models]",
    "apiVars": {
      "#machineId": "$[0].machineId"
    },
    "accessPath": "$[*]",
    "accessMap": {
      
    }
  },
  "getMachineSensors": {
    "api": "http://52.224.181.203:5000/api/machine/#machineId/sensorHistories",
    "apiVars": {
      "#machineId": "$[0].machineId"
    },
    "accessPath": "$[*]",
    "accessMap": {
      
    }
  }
};


```

Calling functions

```javascript

machines = await getDao([{
  conf: 'getAllMachines'
}]);

companies = await getDao([{
  conf: 'getAllCompanies'
}]);

await deleteDao({
  conf: 'deleteMachine',
  machineId: req.params.id
});

const machine = await getDao([{
  conf: 'getSingleMachine',
  machineId: req.params.id
}]);

const sensor = await getDao([{
  conf: 'getMachineSensors',
  machineId: req.params.id
}]);

```

Function definitions

```javascript

const bytenode = require('bytenode');
const Dao = require('../dao/dao_js.jsc');


export const getDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.getGenericWOLoop(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const postDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.requestPost(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const putDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.putcall(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const deleteDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.deletecall(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

```

### Test reports

![](test/e2e/screenshots/reports/test_report_summary.png)

![](test/e2e/screenshots/reports/test_part_1.png)
![](test/e2e/screenshots/reports/test_part_2.png)