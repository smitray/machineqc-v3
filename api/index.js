const express = require('express');
const bodyParser = require('body-parser');
// Create express instnace
const app = express();

// Require API routes
const machines = require('./routes/machines');

app.use(bodyParser.json());
// Import API Routes
app.use(machines);

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
};
