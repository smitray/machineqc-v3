export default (data, pageNum, pageSize) => {
  // const offset = (pageNum - 1) * pageSize;
  // return _.drop(data, offset).slice(0, pageSize);
  const page = pageNum || 1;
  const perPage = pageSize || 5;
  const offset = (page - 1) * perPage;
  return data.slice(offset).slice(0, perPage);
};
