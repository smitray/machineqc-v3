import Fuse from 'fuse.js';

export const pageStatus = (machines, state) => {
  let machine;
  const status = [{
    key: 'registered',
    value: -5
  }, {
    key: 'qc_ready',
    value: 0
  }, {
    key: 'qc_started',
    value: 1
  }, {
    key: 'qc_restarted',
    value: 10
  }, {
    key: 'qc_stopped',
    value: 20
  }, {
    key: 'qc_result_available',
    value: 200
  }, {
    key: 'qc_passed',
    value: 21
  }, {
    key: 'qc_failed',
    value: 22
  }, {
    key: 'transferred',
    value: 3
  }];

  if (state === 'qc') {
    machine = machines.filter(item => item.state !== 3);
  } else {
    const { value } = status.find(item => item.key === state);
    machine = machines.filter(item => item.state === value);
  }
  return machine;
};

export const pageSearch = (machines, search, field) => {
  const fuse = new Fuse(machines, {
    shouldSort: true,
    threshold: 0.4,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 4,
    keys: [
      field
    ]
  });
  return fuse.search(search);
};
