// eslint-disable-next-line no-unused-vars
const bytenode = require('bytenode');
const Dao = require('../dao/dao_js.jsc');


export const getDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.getGenericWOLoop(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const postDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.requestPost(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const putDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.putcall(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});

export const deleteDao = config => new Promise((resolve, reject) => {
  const dao = new Dao();
  dao.deletecall(config).then((result) => {
    resolve(result);
  }).catch((err) => {
    reject(err);
  });
});
