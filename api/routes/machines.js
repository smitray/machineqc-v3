import { format } from 'date-fns';


import {
  paginator,
  getDao,
  deleteDao,
  pageSearch,
  pageStatus
} from '../utilities';

const { Router } = require('express');


const router = Router();

let machines = [];
let companies = [];

/* GET users listing. */
router.get('/machines', async (req, res) => {
  const {
    state,
    pageNum,
    pageSize,
    createdOn,
    modelNo,
    vendorName,
    company,
    machineSerialNo
  } = req.query;

  try {
    if (!state) {
      res.status(404).json({
        message: 'State can not be null'
      });
    }
    machines = await getDao([{
      conf: 'getAllMachines'
    }]);

    companies = await getDao([{
      conf: 'getAllCompanies'
    }]);

    machines = machines.map(machine => ({
      ...machine,
      company: companies.find(cmp => cmp.id === machine.companyId)
    }));

    machines = pageStatus(machines, state);
    if (createdOn) {
      machines = machines.filter(item => format(item.createdOn, 'YYYY-MM-DD') === format(createdOn, 'YYYY-MM-DD'));
    }

    if (modelNo) {
      machines = pageSearch(machines, modelNo, 'machineSku');
    }

    if (machineSerialNo) {
      machines = pageSearch(machines, machineSerialNo, 'serailNumber');
    }

    if (vendorName) {
      machines = pageSearch(machines, vendorName, 'vendorName');
    }

    if (company && state === 'transferred') {
      machines = pageSearch(machines, company, 'company.name');
    }

    const total = machines.length;
    machines = paginator(machines, pageNum, pageSize);
    res.json({
      machines,
      total
    });
  } catch (error) {
    res.status(500).json({
      message: error.message
    });
  }
});


router.delete('/machines/:id', async (req, res) => {
  try {
    await deleteDao({
      conf: 'deleteMachine',
      machineId: req.params.id
    });
    res.json({
      success: true
    });
  } catch (err) {
    res.status(500).json({
      message: err.message
    });
  }
});

router.get('/machines/:id', async (req, res) => {
  try {
    const machine = await getDao([{
      conf: 'getSingleMachine',
      machineId: req.params.id
    }]);
    const sensor = await getDao([{
      conf: 'getMachineSensors',
      machineId: req.params.id
    }]);
    res.json({
      machine,
      sensor
    });
  } catch (err) {
    res.status(500).json({
      message: err.message
    });
  }
});

module.exports = router;
