// eslint-disable-next-line import/no-extraneous-dependencies
import webpack from 'webpack';

export default {
  mode: 'universal',
  modern: 'server',
  head: {
    title: 'Welcome to',
    titleTemplate: titleChunk => (titleChunk ? `${titleChunk} - MachineSense` : 'MachineSense'),
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Machine QC portal' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    '~/assets/style/app.scss'
  ],
  loading: { color: '#3B8070' },
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        _: 'lodash'
      })
    ],
    extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    }
  },
  modules: [
    '@nuxtjs/axios'
  ],
  devModules: ['@nuxtjs/vuetify'],
  axios: {
    baseURL: '/api/'
  },
  vuetify: {
    theme: {
      light: {
        primary: '#FFF',
        accent: '#7b1fa2'
      }
    }
  },
  serverMiddleware: ['~/api/index.js']
};
